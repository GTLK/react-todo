import React from 'react'
import PropTypes from 'prop-types'
import Todo from "./Todo";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import {Image, Work} from "@material-ui/icons";
import ListItemText from "@material-ui/core/ListItemText";

const TodoList = ({deleteTodo, todos}) => {
    return (


    <List>
        {
            todos.map(todo => <Todo key={todo.id} id={todo.id} text={todo.text} completed={todo.completed} deleteTodo={deleteTodo}/>)
        }
    </List>
    )
}

TodoList.propTypes = {
    todos: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            text: PropTypes.string.isRequired,
            completed: PropTypes.bool.isRequired
        }).isRequired
    ).isRequired
}

export default TodoList
