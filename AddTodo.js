import React, {useState} from 'react'
import PropTypes from 'prop-types'
import {Button, TextField, Grid} from '@material-ui/core'
import Container from "@material-ui/core/Container";

const AddTodo = ({onClick}) => {
    const [input, setInput] = useState('')

    return (
        <div>
            {/*<input type="text"*/}
            {/*       onChange={(event) => setInput(event.target.value)}/>*/}
            {/*<button style={{padding: 10, borderRadius: 10, margin: 10, border: 'none'}}*/}
            {/*        >Add Todo*/}
            {/*</button>*/}
            <Container maxWidth="sm">
                <Grid container spacing={4}>
                    <Grid item xs={3}>
                        <TextField
                            fullWidth={true}
                            label="Todo"
                            onChange={(event) => setInput(event.target.value)   }
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <Button  variant="contained" color="primary" onClick={() => onClick(input, false)}>
                            Add Todo
                        </Button>
                    </Grid>
                </Grid>
            </Container>






        </div>
    )
}

AddTodo.propTypes = {
    onClick: PropTypes.func.isRequired
}

export default AddTodo
