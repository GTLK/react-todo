import React, {Component} from 'react'
import PropTypes from 'prop-types'
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import {Delete, Image} from "@material-ui/icons";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";

// class Todo extends Component {
//     render() {
//         return
//     }
// }

const Todo = ({id, text, completed, deleteTodo}) => {

    return <ListItem>
        <ListItemAvatar>
            <Avatar>
                <Image />
            </Avatar>
        </ListItemAvatar>
        <ListItemText primary={text} secondary={completed ? 'Completed' : 'Incomplete'} />
        <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="delete" onClick={() => deleteTodo(id)}>
                <Delete />
            </IconButton>
        </ListItemSecondaryAction>
    </ListItem>
}

// function Todo({text, completed}) {
//     return 'Something'
// }

Todo.propTypes = {
    text: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired
}

export default Todo