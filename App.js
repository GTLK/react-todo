import React, { Component } from 'react';
import './App.css';
import TodoList from "./TodoList";
import AddTodo from "./AddTodo";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {Menu} from "@material-ui/icons";
import axios from 'axios';

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            lastId: 3,
            todos: [
                {
                    id: 0,
                    text: 'Do a react session',
                    completed: true,
                },
                {
                    id: 1,
                    text: 'Do a spring session',
                    completed: true
                },
                {
                    id: 2,
                    text: 'Do a spring security session',
                    completed: false
                },
                {
                    id: 3,
                    text: 'Do a jwt session',
                    completed: false
                }
            ]
        }
    }

    componentDidMount() {
        axios.get('https://reqres.in/api/users?page=2').then(response => {
            response.data.data.map(user => this.setState(previousState => ({
                todos: [
                    ...previousState.todos,
                    {
                        id: previousState.lastId + 1,
                        text: user.email,
                        completed: false
                    }
                ],
                lastId: previousState.lastId + 1
            })))
        })
    }

    addTodo = (text, completed) => {
        this.setState(previousState => ({
            todos: [
                ...previousState.todos,
                {
                    id: previousState.lastId + 1,
                    text,
                    completed
                }
            ],
            lastId: previousState.lastId + 1
        }))
    }

    deleteTodo = (id) => {
        let todos = this.state.todos
        todos.splice(todos.findIndex(e => e.id === id), 1)
        console.log('Here' + id)
        this.setState(previousState => ({
            todos: todos
        }))
    }

    render() {
        return (
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" color="inherit" aria-label="menu">
                            <Menu />
                        </IconButton>
                        <Typography variant="h6">
                            News
                        </Typography>
                        <Button color="inherit">Login</Button>
                    </Toolbar>
                </AppBar>
                <TodoList deleteTodo={this.deleteTodo} todos={this.state.todos}/>
                <AddTodo onClick={this.addTodo}/>
            </div>
        );
    }
}

export default App;
